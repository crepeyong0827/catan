#ifndef __TITLES_SCENE_H__
#define __TITLES_SCENE_H__

#include "cocos2d.h"
#include "testBasic.h"


class TileDemo : public cocos2d::CCLayer
{
protected:
    cocos2d::CCLabelTTF* m_label;
    cocos2d::CCLabelTTF* m_subtitle;
    
public:
    TileDemo(void){}
    virtual ~TileDemo(void){}
    
//    virtual std::string title();
//    virtual std::string subtitle();
    virtual void onEnter(){cocos2d::CCLayer::onEnter();}
    
    void restartCallback(CCObject* pSender);
    void nextCallback(CCObject* pSender);
    void backCallback(CCObject* pSender);
    
//    virtual void ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent){};
};


class TMXHexTest : public TileDemo
{
public:
    TMXHexTest(void);
    virtual std::string title();
};



class TitleScene : public cocos2d::CCLayer
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
    
    // a selector callback
    void menuCloseCallback(CCObject* pSender);
    
    // implement the "static node()" method manually
    CREATE_FUNC(TitleScene);
};

#endif // __TITLES_SCENE_H__
