//
//  GameDef.h
//  catan
//
//  Created by creyong-note on 2014/03/27.
//
//

#ifndef catan_GameDef_h
#define catan_GameDef_h

// ゲームの解像度
#define GAME_RESOLUTION_WIDTH   (480)
#define GAME_RESOLUTION_HEIGHT  (320)

#endif
